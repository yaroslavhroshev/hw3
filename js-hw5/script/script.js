const createNewUser = () => {

    let firstName = prompt('Введіть Ваше ім\'я');
    let lastName = prompt('Введіть Ваше прізвище');
    const newUser = {
        firstName,
        lastName,
        getLogin() {
            return firstName.slice(0, 1).toLowerCase() + lastName.toLowerCase();
        },
        setFirstName() {
            Object.defineProperty(newUser, "firstName", {
                writable: true
            })
            this.firstName = prompt("Введіть Ваше ім'я");
            Object.defineProperty(newUser, "firstName", {
                writable: false
            })
            return this.firstName;
        },
        setLastName() {
            Object.defineProperty(newUser, "lastName", {
                writable: true
            })
            this.lastName = prompt("Введіть Ваше прізвище");
            Object.defineProperty(newUser, "lastName", {
                writable: false
            })
            return this.lastName;
        },
    };
    Object.defineProperty(newUser, 'firstName', {
        writable: false
    })
    Object.defineProperty(newUser, 'lastName', {
        writable: false
    })
    return newUser;


};
const newUser = createNewUser();
console.log(newUser);
console.log(newUser.getLogin());
newUser.setFirstName();
newUser.setLastName();
console.log(newUser);

// Теоретична частина
// 1. Які існують типи даних у Javascript?

// -string
// -number
// -boolean 
// -undefined
// -null
// -object (функції, масиви, об'єкти)
// -symbol

// 2. У чому різниця між == і ===?

// == - знак несуворої рівності. Під час порівняння цим
// оператором завжди відбувається неявне перетворення
// типів, за рахунок чого різні типи даних можуть бути
// рівними один одному. 
// === - знак суворої рівності. Під час порівняння цей
// оператор не перетворює типи, а порівнює дані такими,
// якими вони є.

// 3. Що таке оператор?

// Оператор - це спеціальний символ, який виконує певну
// дію (в залежності від своїх властивостей) над операндами.
// Оператор може бути унарним, бінарним і тернарним.



let userName;
let userAge;

do {
    userName = prompt('What is your name?', userName ? userName.trim() : "");
} while (!userName || !isNaN(userName) || userName == "null") 

do {
    userAge = +prompt('How old are you?', userAge ? userAge.trim() : "");
} while (!userAge || isNaN(userAge));

if (userAge < 18) {
    alert('You are not allowed to visit this website');
} else if (userAge >= 18 && userAge <= 22) {
    let userChoice = confirm('Are you sure you want to continue?')
    if (userChoice === true) {
        alert('Welcome, ' + userName.trim());
    } else {
        alert('You are not allowed to visit this website');
    }
} else {
    alert('Welcome, ' + userName.trim());
}


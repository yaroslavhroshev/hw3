/* Напишіть, як ви розумієте рекурсію. Навіщо вона використовується на практиці?
Рекурсія - це функція, яка, окрім зовнішнього виклику, викликає себе ж всередині свого тіла функції,
тобто вона наче замикається утворюючи цикл. За великим рахунком рекурсія і є цикл, тільки викликаний 
через самовиклик функції. Якщо не поставити умову виходу із рекурсії, то вона умовно буде "вічною",
як цикл while(), де немає умови виходу з циклу.
На практиці, за допомогою рекурсії можна порахувати, перебрати типи даних великої вкладеності, наприклад як
об'єкти, які містять масиви, в яких є об'єкти, тощо. Це можна зробити також і за допомогою циклів,
але в даному випадку в рекурсійному рішенні буде меньше коду і він буде більш читабельним та
зрозумілим.
*/ 

/**
 * @description - ця функція для перевірки валідності введеного користувачем значення.
 * @param {number} number 
 * @returns {boolean}
 */
let validateNumber = number => !(!number || isNaN(number));

/**
 * @description - ця функція для отримання від користувача певного числового значення.
 * @param {string} message 
 * @returns {number} 
 */
let getNumber = (message = "Введіть будь-яке число") => {
    let userNumber;
    do {
        userNumber = prompt(message, userNumber ? userNumber : "");
        if (userNumber !== null) {
            userNumber = userNumber.trim();
        }
    } while(!validateNumber(userNumber))
    return +userNumber
};
/**
 * @description - функція розраховування факторіалу.
 * @param {number} someNumber 
 * @returns {number}
 */
let factorial = function(someNumber) {
    if (someNumber <= 0) return 1

    return someNumber * factorial(someNumber - 1) 
};
let fact = factorial(getNumber());
document.querySelector('p').innerHTML = `Факторіал введенного користувачем числа: ${fact}`;
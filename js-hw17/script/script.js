const checkNumber = number => !(!number || isNaN(number));
let name;
let lastName;
do {
    name = prompt('Вкажіть ім\'я студента');
    lastName = prompt('Вкажіть призвіще студента');
    if (name !== null && lastName !== null) {
        name = name.trim();
        lastName = lastName.trim();
    }
} while (!name || !lastName || !isNaN(name) || !isNaN(lastName))

const student = {
    name,
    lastName,
};
let discipline;
let mark = 0;
student.tabel = {};
do {
    discipline = prompt("Вкажіть назву предмета");
    if (discipline) {
        discipline = discipline.trim();
        if (discipline === "") continue;
    
        do {
            mark = prompt(`Вкажіть оцінку з предмету : ${discipline}`);
            if (mark !== null) {
                mark = mark.trim();  
            }
            student.tabel[discipline] = +mark;
        } while(!checkNumber(mark))   
    }      
} while(discipline !== null);

let checkMark = 0;
let count = 0;
let averScore = 0;
for (let key in student.tabel) {
    checkMark = student.tabel[key] < 4 ? checkMark + 1 : checkMark;
    count++;
    averScore += student.tabel[key];
} 
checkMark = checkMark > 0 ? checkMark : alert("Студента переведено на наступний курс");
averScore = (averScore / count) > 7 ?  alert('Студенту призначено стипендію') : averScore / count;
const form = document.querySelector('.password-form');
const label = document.querySelectorAll('.input-wrapper');

const error = document.createElement('span')
error.innerText = 'Потрібно ввести однакові значення'
error.classList.add('error', 'hidden')
label[1].append(error)

label.forEach(elem => {
    
    const input = elem.querySelector('input')

    elem.addEventListener('click', e => {

        if(e.target.classList.contains('icon-password')) {
            elem.querySelector('.hidden').classList.remove('hidden')
            e.target.classList.add('hidden');
            input.type === 'password' ? input.type = 'text': input.type = 'password'
        }
      
    })
})

form.addEventListener('submit', e => {
    e.preventDefault()
    const inputs = e.target.querySelectorAll('input');

    if (inputs[0].value === inputs[1].value) {
        alert('You are welcome')
        error.classList.add('hidden')
        e.target.querySelectorAll('.fa-eye').forEach(i => i.classList.remove('hidden'))
        e.target.querySelectorAll('.fa-eye-slash').forEach(i => i.classList.add('hidden'))
        inputs.forEach(input => input.type = 'password')
        e.target.reset()
    } else {
        error.classList.remove('hidden')
    }
})

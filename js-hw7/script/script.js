/* Теоретичне завдання
1. Опишіть своїми словами як працює метод forEach.
 Метод forEach() є аналогом циклів for. Працює він таким чином: 
 цей метод викликає callback функцію для кожного елемента масиву. 
 У callback функції існують три параметра, які в неї передаються - це
 element (безпосередньо поточний елемент масиву), index (індекс поточного елемента масиву),
 array (масив, який перебирається). В callback функції прописується якийсь код - який 
 використовується по відношенню до поточного елемента масиву.
2. Як очистити масив?
Є декілька способів:
- let arr = [1, 2, 3, 4, 5, 'somestring'];
  arr.length = 0;
  consol.log(arr) // []
- let arr1 = [1, 2, 3, 4, 5, 'somestring'];
  arr1.splice(0, arr1.length);
  consol.log(arr1) // []
3. Як можна перевірити, що та чи інша змінна є масивом?
Для перевірки значення на відповідність до значення масив є спеціальний метод 
Array.isArray(arr) - якщо значення в дужкає є масивом, то поверне true, якщо ні,
то поверне false.
*/
//   Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
// Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий
// другим аргументом. Тобто якщо передати масив ['hello', 'world', 23, '23', null], і другим аргументом передати 'string', 
// то функція поверне масив [23, null].


const someArr = [
    {name: "Jonh",
     age: 19,
     car: "Mazda"},
    237,
    null,
    true,
    "Victoria",
    {name: "Yaroslav",
     age: 29,
     car: "Lanos"},
     false,
    ["1 str", "2 str", "3 str"],
    "Paolo Sorrentino",
    null,
    () => {
        console.log("I'm here");
    }
]

console.log(someArr);

const filterBy = (arr, typeofValue) => {
    
    return arr.filter(elem => {
        if(typeofValue.toLowerCase() === "object"){
           return elem === null ? true : typeof elem !== typeofValue.toLowerCase();
        } 
        return typeofValue.toLowerCase() === 'null' ? elem !== null : typeof elem !== typeofValue.toLowerCase()
    })
};

console.log(filterBy(someArr, "string"));
let buttons = document.querySelectorAll('.tabs-title');
let arrButtons = Array.from(buttons);
let desc = document.querySelectorAll('.tabs-content > li');
for(let li of desc) {
    li.style.display = "none";
}

arrButtons.forEach(function (elem) {

    elem.addEventListener('click', (e) => {
        for(let button of arrButtons) {
            if(e.target !== button) {
                if (button.classList.contains("active")) {
                    button.classList.remove('active');
                }
            }
            }
            for(let li of desc) {
                if(li.style.display === "") {
                    li.style.display = 'none';
                } else if(e.target.dataset.name === li.dataset.inform) {
                    li.style.display = "";                    
                } else if(e.target.classList.contains("active")) {
                        li.style.display = 'none'
                    }
            }
            e.target.classList.toggle("active");
            
})
})

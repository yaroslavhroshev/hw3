// Теоретична частина
//1 . Описати своїми словами у кілька рядків, навіщо у програмуванні потрібні цикли.
// Якщо нам потрібно перебрати певний масив, або набір вхідних чисел, або взагалі будь яких даних,
// то використовується цикл. До прикладу скажу, що, якщо ми маємо на вхід 100 якихось чисел,
// з  яких нам потрібно за певними критеріями вибрати 20 чисел, то в такому випадку і використовується масив,
// щоб не робити операцію по відбору необхідних нам значень вручну, перевіряючи власноруч кожне
// число зі 100 вхідних.

// 2. Опишіть у яких ситуаціях ви використовували той чи інший цикл в JS.

// Для якихось лічильників, або для пребору значень масива, або ж для перебору вхідних чиселя використовував 
// цикл for. Для створення перевірки на валідність введених користувачем даних - я використовував цикл while.

// 3. Що таке явне та неявне приведення (перетворення) типів даних у JS?

// Явне приведення типів - це коли ми окремою командою (parseInt, Number(), Boolean(), String() тощо),
// умовно кажучи навмисно, перетворюємо один тип даних на інший (чи то string в boolean, чи то boolean в number тощо).
// Неявне приведення типів відбувається під час певних операцій, які за замовчуванням мають властивість змінювати 
// тип даних. До таких операторів відносяться: математичні оператори (+ , -, /, *), оператор несуворої рівності або
// не рівності (==, !=), тощо.

let someNumber;
while (!someNumber || isNaN(someNumber) || !Number.isInteger(someNumber)) {
    someNumber = +prompt('Enter some number');
}


let x = 0;
for (let i = 0; i <= someNumber; i++) {
    if (i % 5 !== 0 || i === 0) {
        continue;
    } else {
        x += 1;
        console.log(i);
    }
}

if (x === 0) {
        console.log('Sorry, no numbers');
}


// Завдання просунутої складності - варіант 1

    // let m = +prompt('Enter the first number');
    // let n = +prompt('Enter the second number');

    // while (!m || !n || isNaN(m) || isNaN(n) || !Number.isInteger(m) || !Number.isInteger(n) || m > n) {
    //     alert('Error. Enter the numbers again');
    //     m = +prompt('Enter the first number');
    //     n = +prompt('Enter the second number');
    // }

    // for (let i = m; i <= n; i++) {
    //     let flag = 1;
    //     for (let j = 2; (j < i)&&(flag == 1); j++ ){
    //         if (i % j === 0) {
    //             flag = 0;
    //         }
    //     }
    //    if (flag == 1) {
    //     console.log(i);
    //    }
    // }

// Завдання просунутої складності - варіант 2

    // let m = +prompt('Enter the first number');
    // let n = +prompt('Enter the second number');

    // while (!m || !n || isNaN(m) || isNaN(n) || !Number.isInteger(m) || !Number.isInteger(n) || m > n) {
    //     alert('Error. Enter the numbers again');
    //     m = +prompt('Enter the first number');
    //     n = +prompt('Enter the second number');
    // }
    // loop1:
    // for (let i = m; i <=n; i++) {
    //     loop2:
    //     for (let j = 2; j < i; j++) {
    //         if(i % j === 0) {
    //             continue loop1;
    //          } 
    //      }
    //       console.log(i);
    // }
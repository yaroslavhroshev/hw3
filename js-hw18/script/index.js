const cloneObj = object => {
    let clone = {};
    for(let key in object) {
        if (typeof object[key] === "object" && object[key] !== null) {
            if (object[key] instanceof Array) {
             clone[key] = Array.from(object[key]);
             console.log(clone[key]);
            } else {
                clone[key] = cloneObj(object[key]);
                continue
            }
        } 
        clone[key] = object[key];
    } return clone;
}


const student = {
    name: 'Yaroslav',
    lastName: 'Hroshev',
    profession: 'Front-End dev',
    hobby: {
        sport: "workout",
        language: "english",
        animal: "cat",
        books: [
            {firstBook: "Бідний батько, багатий батько"},
            {secondBook: "Володар кілець"},
            {thirdBook: "Відьмак"},
        ],
        games: {
            rollGames: ['Dnd', 'Pasfinder', 'The Masquerade of vampires'],
            survival: "The king of Tokio",
            wargaming: "Warhummer 40000",
        },
    },
    getFullName() {
        return `${this.name} ${this.lastName}`;
    },
};
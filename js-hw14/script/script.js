const changeStyleButton = document.createElement('button');
changeStyleButton.innerText = 'Змінити тему';
changeStyleButton.classList.add('change-btn')
document.querySelector('.main').append(changeStyleButton)


const mainPage = document.querySelector('.main-page');
const facebookBtn = document.querySelector('.facebook-button');
const googleBtn = document.querySelector('.google-button');
const createAccountBtn = document.querySelector('.create-account-button')


const mainStyles = JSON.parse(localStorage.getItem("mainStyle"));

if (localStorage.getItem('mainStyle')) {
    mainPage.style.backgroundImage = mainStyles.mainPage;
    facebookBtn.style.backgroundColor = mainStyles.facebookBtn;
    googleBtn.style.backgroundColor = mainStyles.googleBtn;
    changeStyleButton.style.backgroundColor = mainStyles.changeStyleButton;
    createAccountBtn.style.backgroundColor = mainStyles.createAccountBtn;
}


const secondStyle = {
    mainPage: "url('https://picsum.photos/1366/768')",
    facebookBtn: 'aqua',
    googleBtn: 'yellow',
    changeStyleButton: 'black',
    createAccountBtn: 'purple',
}

const mainStyle = {
    mainPage: "url('../image/Background.png')",
    facebookBtn: "rgb(48, 93, 184)",
    googleBtn: "rgb(223, 74, 50)",
    changeStyleButton: "rgba(60, 184, 120, 0.8)",
    createAccountBtn: "rgb(48, 93, 184)",
}


changeStyleButton.addEventListener('click', () => {
    
    let mainBack = getComputedStyle(mainPage).backgroundImage;

    if (mainBack.includes('/image/Background.png')) {
        
        localStorage.setItem('mainStyle', JSON.stringify({
                mainPage: secondStyle.mainPage,
                facebookBtn: secondStyle.facebookBtn,
                googleBtn: secondStyle.googleBtn,
                changeStyleButton: secondStyle.changeStyleButton,
                createAccountBtn: secondStyle.createAccountBtn,
            }))
        
        mainPage.style.backgroundImage = secondStyle.mainPage;
        facebookBtn.style.backgroundColor = secondStyle.facebookBtn;
        googleBtn.style.backgroundColor = secondStyle.googleBtn;
        changeStyleButton.style.backgroundColor = secondStyle.changeStyleButton;
        createAccountBtn.style.backgroundColor = secondStyle.createAccountBtn;
    } else {

        localStorage.setItem('mainStyle', JSON.stringify({
                mainPage: mainStyle.mainPage,
                facebookBtn: mainStyle.facebookBtn,
                googleBtn: mainStyle.googleBtn,
                changeStyleButton: mainStyle.changeStyleButton,
                createAccountBtn: mainStyle.createAccountBtn,
            }))
            mainPage.style.backgroundImage = mainStyle.mainPage;
            facebookBtn.style.backgroundColor = mainStyle.facebookBtn;
            googleBtn.style.backgroundColor = mainStyle.googleBtn;
            changeStyleButton.style.backgroundColor = mainStyle.changeStyleButton;
            createAccountBtn.style.backgroundColor = mainStyle.createAccountBtn;
    }
})
const dropdownMenu = document.querySelector('.header__dropdown-menu');
const burgerMenu = document.querySelector('.header__burger-menu');
const navBar = burgerMenu.parentElement;

navBar.addEventListener('click', e => {
    e.preventDefault();
    if (e.target === e.currentTarget) return
    dropdownMenu.classList.remove('hidden')
})

burgerMenu.addEventListener('mouseleave', () => {
    dropdownMenu.classList.add('hidden')
})

dropdownMenu.addEventListener('mouseleave', e => {
    e.target.classList.add('hidden')
})



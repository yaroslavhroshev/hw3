import gulp from "gulp";
import htmlmin from "gulp-htmlmin";
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
const sass = gulpSass(dartSass);
import browserSync from "browser-sync";
import clean from "gulp-clean";
import cleanCSS from "gulp-clean-css";
import concat from "gulp-concat";
import terser from "gulp-terser";
import imagemin from 'gulp-imagemin';
import autoprefixer from 'gulp-autoprefixer';


const html = () => {
    return gulp.src('./src/*.html')
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest('./dist/'))
}

const cleanHtml = () => {
    return gulp.src('./dist/*.html', {read: false})
	.pipe(clean());
}

function scss() {
    return gulp.src('./src/scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./dist/css'));
}

 
const css = () => {
    return gulp.src('./dist/css/*.css')
    .pipe(autoprefixer({
        cascade: false
    }))
    .pipe(cleanCSS())
    .pipe(gulp.dest('./dist/css'))
}

const cleanCss = () => {
    return gulp.src('./dist/**/*.css', {read: false})
    .pipe(clean());
}

const js = () => {
    return gulp.src('./src/js/*.js')
    .pipe(concat('all.js'))
    .pipe(terser())
    .pipe(gulp.dest('./dist/js'));
}

const cleanJS = () => {
    return gulp.src('./dist/**/*.js')
    .pipe(clean())
}

const cleanDist = () => {
    return gulp.src('./dist', {read: false})
    .pipe(clean())
}

function imgMin (next) {
    gulp.src('./src/images/**/*')
    .pipe(imagemin())
	.pipe(gulp.dest('./dist/images'))
    next()
}

const bs = browserSync.create();

const dev = () => {
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });

    gulp.watch('./src/*.html', gulp.series(cleanHtml, html, (next) => {
        browserSync.reload()
        next()
    } ))

    gulp.watch('./src/**/*.scss', gulp.series(cleanCss, scss, css, (next) => {
        browserSync.reload()
        next()
    }))

    gulp.watch('./src/**/*.js', gulp.series(cleanJS, js, (next) => {
        browserSync.reload()
        next()
    }))
}


gulp.task('build', gulp.series(cleanDist, gulp.parallel(html, gulp.series(scss, css), js, imgMin)))
gulp.task('dev', dev)
/* Теоретичне завдання:
1. Опишіть, як можна створити новий HTML тег на сторінці.
 Новий елемент на сторінці можно створити динамічно з Javascript за допомогою методу document.createElement(tagname).
 Цей метод записуємо в змінну, яка буде зберігати в собі посилання на об'єктне уявлення цього елемента. Після цього ми
 можемо прописати різні властивості, атрибути та значення атрибутів и додати цей елемент в HTML-розмітку за допомогою 
 методів вставки : append(), prepend(), тощо.
 Також для створення елемента можно використати методи innerHTML та insertAdjacentHTML. Вони парсять рядок зчитуючи
 HTML код записаний в рядку та додаючи його в DOM.
2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
Перший параметр цієї функції вказує куди потрібно вставити HTML відносно елемента, на якому викликається цей метод.
В першому параметрі нам доступно чотири варіанти параметру: "afterbegin", "afterend", "beforebegin", "beforeend".
"afterbegin" - цей параметр вставляє HTML одразу після відкриваючого тега елемента, на якому викликається цей метод.
Цей варіант робить вставлений HTML першим дочірнім елементом елемента, на якому викликається цей метод.
"afterend" - цей параметр вставляє HTML одразу після закриваючого тега елемента, на якому викликається цей метод. 
Тобто він вставляє HTML одразу після елемента, на якому викликається цей метод, стаючи при цьому сусіднім HTML-елементом/кодом тощо.
"beforebegin" - цей параметр вставляє HTML перед елементом, на якому викликається цей метод.
"beforeend" - цей параметр вставляє HTML перед закриваючим тегом елемента, на якому викликається цей метод. HTML-код стає
останнім дочірнім елементом елемента, на якому викликається цей метод.
3. Як можна видалити елемент зі сторінки?
Для видалення елемента зі сторінки існує метод remove(), який викликається на елементі, який потрібно видалити.
Але для видалення елемента його потрібно спочатку знайти. 
Також для видалення елемента зі сторінки можна використовувати метод innerHTML. Цей метод перезаписує вміст HTML-елемента,
і якщо там були HTML-теги, то цей метод перед записуванням свого HTML-коду - видаляє попередній код. Тобто елементи які були раніше 
в елементі, на якому викликається innerHTML будуть видалені.
*/ 



const firstArr =  ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const secondArr = ["1", "2", "3", "sea", "user", 23];
const thirdArr = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

const showToDoList = (array, parent = document.body) => {

const createToDoList = array => {

    const ul = document.createElement('ol');
    array.forEach(elem => {
        const li = document.createElement('li');
        
        if (Array.isArray(elem)) {
            li.append('Task list:', createToDoList(elem))
        } else {
            li.innerText = elem
        }
       
        ul.append(li)
    })
    
    return ul
}
   parent.append(createToDoList(array))

}

const container = document.querySelector('.container');
showToDoList(thirdArr, container);
showToDoList(secondArr);
showToDoList(firstArr, container);


let i = 3;
let timeCounter = document.createElement('p');
document.body.append(timeCounter)
let pageCleaner =  setTimeout(function timer() {
    timeCounter.innerHTML = '';
    timeCounter.innerText = `${i} second`;
    if (i === 0) {
        document.body.innerHTML = ""
        clearTimeout(pageCleaner)
    }
    i--;
    pageCleaner = setTimeout(timer, 1000)
}, 1000);




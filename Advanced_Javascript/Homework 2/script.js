const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

class ObjectPropertyError extends Error {
    constructor(key) {
        super()
        this.name = 'ObjectPropertyError';
        this.message = `This object cannot be rendered. The "${key}" property is missing`;
    }
}


class List {
    constructor(book) {
        this.book = book;
        this.necessaryBookKeys = ['author', 'name', 'price'];
        if (Object.keys(this.book).length !== 3) {
            this.necessaryBookKeys.forEach(key => {
                if (!this.book.hasOwnProperty(`${key}`)) {
                    throw new ObjectPropertyError(key)
                }
            })
        }

        this.listItem = document.createElement('li');
        this.list = document.createElement('ul');

    }

    createElement() {
        this.listItem.innerText = `${this.book.author}: ${this.book.name} (${this.book.price})`;
        this.list.append(this.listItem)
    }

    render(selector) {
        this.createElement()
        document.querySelector(`${selector}`).append(this.list);
    }
}


books.forEach(book => {
    try {
        new List(book).render('#root')
    } catch (err) {
        if (err instanceof ObjectPropertyError) {
            console.error(err);
        } else {
            throw err
        }
    }
})

/*
Теоретична частина:
1. Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
- Коли приходять неповні дані з сервера ми можемо використати конструкцію try...catch для обробки такої події і створення 
запобіжного заходу у вигляді повторної відправки запита на сервер чи ще чогось.
- Під час парсингу JSON можуть виникати помилки, якщо в JSON були записані/перадані дані, які не можуть бути розпарсені. В даному
випадку можна скористатись конструкцією try...catch для того, щоб запобігти сценарію з помилкою яка "завалить" скрипт.
*/
axios.get('https://ajax.test-danit.com/api/json/users')
    .then(({ data }) => {
        const userList = [...data];
        axios.get('https://ajax.test-danit.com/api/json/posts')
            .then(({ data }) => {
                data.map(({ userId, title, body, id }) => {

                    const newUserList = userList.filter(user => user.id === userId);
                    const [{ name, email }] = newUserList;
                    return { title, body, name, email, userId, id }

                }).forEach(({ name, email, body, title, userId, id }) => {
                    new Card(title, body, name, email, userId, id).render('.container')
                })
                document.querySelector('.loader').style.display = 'none';
                document.querySelectorAll('a').forEach(link => {
                    link.addEventListener('click', e => e.preventDefault())
                })
            })
            .catch(error => console.warn(error))
    })
    .catch(error => console.warn(error))


class Card {
    constructor(title, text, name, email, userId, postId) {
        this.title = title;
        this.text = text;
        this.name = name;
        this.email = email;
        this.userId = userId;
        this.postId = postId;
        this.deleteBtn = document.createElement('div');
        this.postTitle = document.createElement('h2');
        this.postText = document.createElement('p');
        this.card = document.createElement('div');
        this.editBtn = document.createElement('div');
        this.editModalWindow = document.createElement('div');
    }

    createElements() {

        const imageWrapper = document.createElement('div');
        imageWrapper.className = 'card__image-wrapper';
        imageWrapper.innerHTML = `<img src="https://picsum.photos/${80 + this.userId}/${80 + this.userId}" alt="user-avatar" class="card__img"></img>`;

        const cardContentWrapper = document.createElement('div');
        cardContentWrapper.className = 'card__content-wrapper';

        const userInfoWrapper = document.createElement('div');
        userInfoWrapper.className = 'card__user-info-wrapper';

        userInfoWrapper.innerHTML = `
        <span class="card__user-name">${this.name}</span>
        <div class="card__icon-wrapper">
        <div class="card__icon card__icon--position-center"></div>
        </div>
        <a href="#" class="card__user-email">${this.email}</a>
        <div class="card__kebab-menu-wrapper">
        <div class="card__kebab-menu"></div>
        </div>`;

        this.editBtn.className = 'card__edit-btn';

        this.deleteBtn.className = 'card__delete-icon';
        userInfoWrapper.append(this.deleteBtn, this.editBtn)

        const postContentWrapper = document.createElement('div');
        postContentWrapper.className = 'card__post-content-wrapper';

        const postTitileWrapper = document.createElement('div');
        postTitileWrapper.className = 'card__post-title-wrapper';
        postTitileWrapper.innerHTML = ` <div class="card__post-icon"></div>`;

        this.postTitle.className = 'card__post-title';
        this.postTitle.textContent = this.title;

        postTitileWrapper.append(this.postTitle);

        this.postText.className = 'card__post-text';
        this.postText.textContent = this.text;

        postContentWrapper.append(postTitileWrapper, this.postText)

        cardContentWrapper.append(userInfoWrapper, postContentWrapper)

        this.card.className = 'card';
        this.card.id = this.postId;

        this.card.append(imageWrapper, cardContentWrapper)
    }

    addListeners() {
        this.deleteBtn.addEventListener('click', () => {
            axios.delete(`https://ajax.test-danit.com/api/json/posts/${this.postId}`)
                .then(({ status }) => {
                    if (status === 200) {
                        this.card.remove()
                    }
                })
                .catch(err => console.warn(err))
        })

        this.editBtn.addEventListener('click', () => {
            changeModalWindow(true)
            changeNewPostModalWindow(false);
            changeEditModalWindow(true)
            document.querySelector('.js-change-btn').addEventListener('click', () => {
                let inputTitle = document.querySelector('input').value;
                let inputBody = document.querySelector('textarea').value;


                axios.put(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
                    title: inputTitle,
                    body: inputBody,
                    userId: this.userId,
                })
                    .then(({ data }) => {
                        this.postTitle.innerText = data.title;
                        this.postText.innerText = data.body;
                        changeModalWindow(false)
                    })
                    .catch(err => console.warn(err))

            })
        })
    }

    render(selector) {
        this.createElements()
        this.addListeners()
        document.querySelector(`${selector}`).prepend(this.card)
    }


}

function clearInputs() {
    document.querySelector('input').value = "";
    document.querySelector('textarea').value = "";
}

function changeModalWindow(boolean) {
    if (boolean) {
        document.querySelector('.modal-window').style.display = 'block';
        clearInputs()
    } else {
        document.querySelector('.modal-window').style.display = 'none'
        clearInputs()
    }
}

function changeDisplay(boolean, ...selectors) {
    if (boolean) {
        selectors.forEach(selector => {
            document.querySelector(`.${selector}`).style.display = 'block';
        })
    } else {
        selectors.forEach(selector => {
            document.querySelector(`.${selector}`).style.display = 'none';
        })
    }
}

function changeNewPostModalWindow(boolean) {
    if (boolean) {
        changeDisplay(true, 'modal-window__btn--accept-btn', 'modal-window__new-post-legend')
    } else {
        changeDisplay(false, 'modal-window__btn--accept-btn', 'modal-window__new-post-legend')
    }
}

function changeEditModalWindow(boolean) {
    if (boolean) {
        changeDisplay(true, 'modal-window__btn--change-btn', 'modal-window__change-post-legend')
    } else {
        changeDisplay(false, 'modal-window__btn--change-btn', 'modal-window__change-post-legend')
    }
}

document.querySelector('.add-post-btn').addEventListener('click', () => {
    changeModalWindow(true)
    changeEditModalWindow(false)
    changeNewPostModalWindow(true);
})


document.querySelector('.js-accept-btn').addEventListener('click', () => {
    let inputTitle = document.querySelector('input').value;
    let inputBody = document.querySelector('textarea').value;

    axios.post('https://ajax.test-danit.com/api/json/posts', {
        title: inputTitle,
        body: inputBody
    })
        .then(({ data }) => {
            const { body, id, title } = data;
            axios.get('https://ajax.test-danit.com/api/json/users/1')
                .then(({ data }) => {
                    const { email, name, id: userId } = data;

                    new Card(title, body, name, email, userId, id).render('.container')
                    changeModalWindow(false)
                })
                .catch(err => console.warn(err))
        })
        .catch(err => console.warn(err))
})

document.querySelector('.modal-window').addEventListener('click', (e) => {
    if (e.target === document.querySelector('.modal-window__close-btn')) {
        changeModalWindow(false)
    }

    if (e.target === document.querySelector('.modal-window__background')) {
        changeModalWindow(false)
    }
})


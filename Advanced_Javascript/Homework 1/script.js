class Employee{
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name
    }

    set name(value) {
        if (value !== null && typeof value === 'string') {
            this._name = value;
        } else {
            console.error('Null or number can\'t be a name!');
        }
    }

    get age() {
        return this._age
    }

    set age(value) {
        if (value !== null && typeof value === 'number') {
            this._age = value;
        } else {
            console.error('This property can only be a number');
        }
    }

    get salary() {
        return this._salary + "$"
    }

    set salary(value) {
        if (value !== null && typeof value === 'number') {
            this._salary = value;
        } else {
            console.error('This property can only be a number');
        }
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary)
        this.lang = [...lang];
    }

    get salary() {
        return this._salary * 3 + '$'
    }
}

const john = new Programmer('Jonh', 31, 1000, ['Javascript', 'HTML', 'CSS']);
const abdul = new Programmer('Abdul', 25, 800, ['HTML', 'CSS','Python', 'PHP']);
const ilonMask = new Programmer('Ilon Mask', 51, 5000000, ['C++', 'C#', 'Python', 'Java', 'Ruby', 'Kotlin', 'Dart', 'TypeScript']);
const namRa = new Programmer('Nam-Ra', 37, 1700, ['Go', 'Groovy', 'Apex', 'Clojure']);
console.log(john);
console.log(abdul);
console.log(ilonMask);
console.log(namRa);

/* Теоретична частина
1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript.
Прототип - це по суті предок, властивості та методи якого наслідують його похідні. Це як батько, гени якого в повній 
мірі наслідують його діти. Прототипне наслідування працює як передача генів від "найстаршого" до "наймолодшого" 
представника цієї гілки. При цьому наймолодшому представнику прототипного успадкування доступні майже всі методи
та властивості, які були представлені у його прототипів (предків).
2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
Для ініціалізації батьківських властивостей (прописаних в батьківському конструкторі) в дочірньому класі, для передачі цих 
даних в дочірній клас та для створення зв'язку між конструкторм батьківського класу та конструктором дочірнього класу.
*/
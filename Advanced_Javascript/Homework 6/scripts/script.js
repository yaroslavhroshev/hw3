document.querySelector('#find-for-ip').addEventListener('click', async () => {
    try {
        const { data: { ip } } = await axios.get('https://api.ipify.org/?format=json');
        console.log(ip);

        const { data: userLocations } = await axios.get(`http://ip-api.com/json/${ip}`);
        console.log(userLocations);

        document.querySelector('.container').insertAdjacentHTML('beforeend', ` 
        <div class="user-location">
            <p>Country: ${userLocations.country}</p>
            <p>Region: ${userLocations.regionName}</p>
            <p>City: ${userLocations.city}</p>
        </div>`)

    } catch (error) {
        console.log(error);
    }

})

/*
Теоретичне завдання
1. Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript.

Асинхронність у Javascript реалізується за допомогою Web API. Це додатковий інструмент, влаштований в браузер, 
за допомогою якого відбувається обробка асинхронних запитів. Сам по собі Javascript не є асинхронною мовою програмування, 
він однопотоковий. І коли в його коді зустрічаються асинхронні запити, вони спочатку потрапляють в stack, після чого
відправляються в Web API, в оточенні якого і реалізується асинхронний код. Після виконання цього коду - відповідь (резуль
тат виконання коду) потрапляє в чергу, і коли stack стає порожнім, тоді Event Loop перекидує відповідь з черги до stack.

*/

// Task 1

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const allClients = [...new Set(clients1, clients2)];

console.log(allClients);

// Task 2

const characters = [
    {
        name: "Елена",
        lastName: "Гилберт",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Кэролайн",
        lastName: "Форбс",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Аларик",
        lastName: "Зальцман",
        age: 31,
        gender: "man",
        status: "human"
    },
    {
        name: "Дэймон",
        lastName: "Сальваторе",
        age: 156,
        gender: "man",
        status: "vampire"
    },
    {
        name: "Ребекка",
        lastName: "Майклсон",
        age: 1089,
        gender: "woman",
        status: "vempire"
    },
    {
        name: "Клаус",
        lastName: "Майклсон",
        age: 1093,
        gender: "man",
        status: "vampire"
    }
];

const charactersShortInfo = characters.map(obj => {
    const { name, lastName, age } = obj;
    return { name, lastName, age }
})

console.log(charactersShortInfo);

// Task 3

const user1 = {
    name: "John",
    years: 30
};

const { name, years: age, isAdmin = false } = user1;

document.querySelector('.show-info').innerHTML = `<p>${name}</p><p>${age}</p><p>isAdmin: ${isAdmin}</p>`;

// Task 4

const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
        lat: 38.869422,
        lng: 139.876632
    }
}

const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
}

const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto',
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
}

const allInfoSatoshi = { ...satoshi2018, ...satoshi2019, ...satoshi2020 };

console.log(allInfoSatoshi);

// Task 5

const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
}, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
}, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
}

const allBooks = [...books, bookToAdd];
console.log(allBooks);

// Task 6

{
    const employee = {
        name: 'Vitalii',
        surname: 'Klichko'
    }

    const newEmployee = {
        ...employee,
        age: 0,
        salary: 15000,
    }
    console.log(newEmployee);
}

// Task 7

const array = ['value', () => 'showValue'];

const [value, showValue] = array;

alert(value); // має бути виведено 'value'
alert(showValue());  // має бути виведено 'showValue'

/* Теоретична частина
Поясніть своїми словами, як ви розумієте, що таке деструктуризація і навіщо вона потрібна.

Деструктурізація - це зручний спосіб діставати данні з об'єктів, масивів тощо. І як наслідок, його синтаксис спрощує
створення нових об'єктів, масивів на основі вже деструктурованих даних (об'єктів, масивів). І в цілому,
синтаксис деструктурізації робить код більш стислим, а маніпулювання інформацією зручніше і швидше.
*/
axios.get('https://ajax.test-danit.com/api/swapi/films')
    .then(({ data }) => {
        data.forEach(({ episodeId, name, openingCrawl, characters, id }) => {
            new StarWarsMovieList(episodeId, name, openingCrawl, characters, id).render('list')
        });
    })
    .catch(err => console.log(err))

class StarWarsMovieList {
    constructor(episodId, name, openingCrowl, characterList, id) {
        this.episodId = episodId;
        this.name = name;
        this.openingCrowl = openingCrowl;
        this.id = id;
        this.movieCard = null;
        this.characterList = characterList;
        this.charactersArr = [];
    }

    movieCharacters() {
        document.querySelectorAll('.loader').forEach(elem => elem.style.display = 'block')

        const newCharacterArr = this.characterList.map(url => fetch(url).then(res => res.json()))
        Promise.allSettled(newCharacterArr)
            .then(arr => {
                const newCharactersArr = arr.filter(({ status }) => status === 'fulfilled').map(({ value }) => value)
                this.charactersArr = newCharactersArr.map(({ name }) => name);

                document.getElementById(`${this.id}`).querySelector('.characters').innerText = "Characters: " + this.charactersArr.join(', ');
                document.getElementById(`${this.id}`).querySelector('.characters').style.display = 'list-item';
                document.getElementById(`${this.id}`).querySelector('.loader').style.display = 'none';
            })
            .catch(err => console.log(err))
    }

    creareElement() {
        this.movieCard = `<li>
                            <ul id="${this.id}">
                                <li>Episod number: ${this.episodId}</li>
                                <li>Name: "${this.name}"</li>
                                <span class="loader"></span>
                                <li style="display: none;" class='characters'></li>
                                <li>Opening crowl: ${this.openingCrowl}</li>
                            </ul>
                         </li>`;
    }

    render(selector) {
        this.creareElement()
        document.querySelector(`.${selector}`).insertAdjacentHTML('beforeend', this.movieCard);
        this.movieCharacters()
    }
}
/* Теоретична частина
1. Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
Це технологія для асинхронного обміну інформацією між сервером та клієнтом (браузером) з можливістю підгружати дані з сервера, 
не перезавантажуючи сторінку, та на основі цих даних динамічно змінювати зміст стрінки. Ця технологія корисна тим, що під час 
користування веб сторінкою - отримуючи дані з сервера сторінка не перезаватажується, тим самим не запускаючи весь скрипт 
з самого початку. Прикладом може бути відправка форми з данними користувача на сервер, без перезавантаження всієї сторінкі, 
або ж дінамічне оновлення якихось данних чи зображень, знову ж таки без перезавантаження всієї сторінки.
 */
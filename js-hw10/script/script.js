const tabs = document.querySelector('.tabs');
const tabsContentList = document.querySelectorAll('.tabs-content li');

tabs.addEventListener('click', event => {
    if (event.target === event.currentTarget) return

    Array.from(tabs.children).forEach(e => {if (e !== event.target) e.classList.remove('active')})
    
    tabsContentList.forEach(e => {
        e.style.display = 'none'
        if(e.dataset.name === event.target.innerText) e.style.display = 'block'
        if(event.target.classList.contains('active')) e.style.display = 'none';
    }) 
   
    event.target.classList.toggle('active')

})

tabs.addEventListener('mousedown', e => {
    e.preventDefault()
}) // щоб не виділявся текст при швидкому подвійному кліку в центрі елемента


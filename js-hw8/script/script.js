/* Теоретичне завдання
1. Опишіть своїми словами що таке Document Object Model (DOM)
Document Object Model - це представлення HTML-сторінки у вигляді дерева об'єктів, де кожний елемент є окремим об'єктом
зі своїми властивостями, до яких можна звернутись і значення яких можна змінити за допомогою різних методів.

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
innerHTML - повністю перезаписує вміст елемента, включаючи HTML-тегі. Також при звертанні до цієї властивості буде відображатися
внутрішній HTML контент елемента. При присвоєнні нового значення через цю властивість - в строкове значення можна вписувати 
HTML-код, браузер буде зчитувати його і він не буде відображатися на екрані як частина строкового значення, але буде присутній 
в HTML-розмітці.
innerText - повністю перезаписує вміст елемента, включаючи HTML-тегі. Але, при звертанні до цієї властивості буде відображатися
текстовий контент елемента, а не HTML-вміст елемента. Також, при присвоєнні нового значення через цю властивість - в строкове значення 
НЕ можна вписувати HTML-код, тому-що браузер буде сприймати його як звичайне строкове значення і він буде відображатися на екрані як 
частина строкового значення. 

3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
Існує декілька способів звернутися до елемента:
element.getElementById() - звернутися до елемента за його id;
element.getElementsByTagName() - звернутися до елементів за допомогою імені тегаж
element.getElementsByClassName() - звернутися до елементів за допомогою назви класа;
element.getElementsByName() - звернутися до елементів за значенням атрибута name;
element.querySelector() -  звернутися до елементу за певним css-селектором;
element.querySelectorAll() - звернутися до елементів за певним css-селектором; 
На мою думку кращі останні два способи звертання до елементів, бо вони більш універсальні і покривають в собі більше 
варіантів звернення до елемента.
*/

const allParagraph = document.querySelectorAll('p');
allParagraph.forEach(elem => {
    elem.style.backgroundColor = '#ff0000';
})
const optionsList = document.getElementById('optionsList');
console.log(optionsList);
const parent = optionsList.parentElement;
console.log(parent);
const childrenNodes = optionsList.childNodes;
const getNodeType = element => element === 3 ? "TEXT_NODE" : "ELEMENT_NODE";
childrenNodes.forEach(elem => {
    console.log(`Element name - ${elem.nodeName}, elem type - ${getNodeType(elem.nodeType)}`);
});
const testParagraph = document.querySelector('#testParagraph');
testParagraph.innerText = 'This is a paragraph';
const li = document.querySelectorAll('.main-header li');
console.log(li);
li.forEach(elem => {
    elem.classList.add('nav-item')
})
const sectionTitleElems = document.querySelectorAll('.section-title');
sectionTitleElems.forEach(elem => {
    elem.classList.remove('section-title');
})
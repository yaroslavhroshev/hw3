const images = document.querySelector('.images-wrapper');
console.log(images.children.length);
let counter = 0;
let intervalId = null;

const stopShowBtn = document.createElement('button');
stopShowBtn.innerText = 'Припинити';

stopShowBtn.addEventListener('click', () => {
    clearInterval(intervalId)
    showBtn.disabled = false;
    clearInterval(miliSecondTimer)
})

const showBtn = document.createElement('button');
showBtn.innerText = 'Відновити показ';
showBtn.disabled = true;

showBtn.addEventListener('click', () => {
  intervalId = setInterval(showImg, 3000)
  showBtn.disabled = true;
  miliSecondTimer = setInterval(timerMiliSec, 100, sec = 2, miliSec = 10)
})

const container = document.createElement('div');
container.append(stopShowBtn, showBtn)
container.className = "container";

const p = document.createElement('p');
p.style.cssText = `position:absolute;
top: 0;
left: 0;`;
document.body.prepend(p);
let miliSecondTimer = null;
let miliSec = 10;
let sec = 2;
let secondTimer = null;

function timerMiliSec() {
   
    p.innerText = `До зміни слайду залишилось ${sec} сек : ${miliSec--} мілісекунд`
    if(miliSec === 0) {
        miliSec = 10;
        --sec
    }
    if (sec === -1) {
        miliSec = 10;
        sec = 2;
    } 
}

miliSecondTimer = setInterval(timerMiliSec, 100)

function showImg() {
    images.querySelector('.showed').classList.remove('showed')
    
    if (!document.querySelector('.container')) {
                images.after(container)
        }

    setTimeout(() => {
        images.children[counter].classList.add('showed')
    }, 500)
    
    counter++
    if (counter === images.children.length) {
        counter = 0
    }
}

intervalId = setInterval(showImg, 3000) 


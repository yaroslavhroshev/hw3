
const buttons = document.querySelectorAll('.btn')
window.addEventListener('keydown', e => {
    const activeBtn = document.querySelector('.btn-wrapper').querySelector('.active')
    buttons.forEach(button => {
        if(button.textContent === e.key || button.textContent.toLowerCase() === e.key) {
            if(activeBtn && activeBtn !== button) {
                activeBtn.classList.remove('active')
            }
            button.classList.toggle('active') 
        }                                                             
    })
})

/* Теоретичне завдання
1. Чому для роботи з input не рекомендується використовувати клавіатуру?
Якщо мова йде про роботу з тегом input і подіями клавіатури то, дійсно, з тегом input краще використовувати інші події: input або change.
У зв'язку з тим, що існують й інші способи вводу текста окрім клавіатури (копіювати і вставляти, або ж взагалі голосовий набір тексту), 
то в такому випадку "навішуючи" події клавіатури на тег input - введеня тексту не з клавіатури не буде оброблятися, або ж можна буде ввести
якісь символи, на які була поставлена заборона введення при введені з клавіатури, тощо. Це робить наш input не універсальним з приводу прийнття та обробки 
текстових значень, або ж реакцій на певні символи. Тому краще використовувати саме події input або change, бо ці події реагують саме на зміну значення 
в тезі input, незалежно від способу введення тексту.
*/

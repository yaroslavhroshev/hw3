const createNewUser = () => {

    let firstName = prompt('Введіть Ваше ім\'я');
    let lastName = prompt('Введіть Ваше прізвище');
    let birthday = prompt("Введіть Вашу дату народження в форматі дд.мм.рррр");
    const newUser = {
        firstName,
        lastName,
        birthday,
        getLogin() {
            return firstName.slice(0, 1).toLowerCase() + lastName.toLowerCase();
        },
        setFirstName() {
            Object.defineProperty(newUser, "firstName", {
                writable: true
            })
            this.firstName = prompt("Введіть Ваше ім'я");
            Object.defineProperty(newUser, "firstName", {
                writable: false
            })
            return this.firstName;
        },
        setLastName() {
            Object.defineProperty(newUser, "lastName", {
                writable: true
            })
            this.lastName = prompt("Введіть Ваше прізвище");
            Object.defineProperty(newUser, "lastName", {
                writable: false
            })
            return this.lastName;
        },
        getAge() {
            let date = Date.now();
            let age = new Date(birthday.slice(6), birthday.slice(3, 5) - 1, birthday.slice(0, 3));
            return parseInt((date - age) / 1000 / 3600 / 24 / 365)
        },
        getPassword() {
            let password = this.firstName.slice(0, 1).toUpperCase() + this.lastName.toLowerCase() + birthday.slice(6);
            return password
        },
    };
    Object.defineProperty(newUser, 'firstName', {
        writable: false
    })
    Object.defineProperty(newUser, 'lastName', {
        writable: false
    })
    return newUser;
};

const user = createNewUser();
console.log(user);
console.log(user.getLogin());
console.log(user.getPassword());
user.setFirstName();
user.setLastName();
console.log(user);
console.log(user.getAge());
console.log(user.getPassword());

/* Теоретичне завдання:
1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування.
Екранування - це метод, який реалізується за допомогою спеціальних символів, який допомогає уникнути помилок в Javascript
під час зчитування ним різних символів та знаків. Наприклад: коли ми беремо строкове значення в одинарні лапки, і в цьому
строковому значенні є знак апострофу, то за замовчуванням Javascript сприймає його як закінчення одинарних лапок, а все, що
буде знаходитись за апострофом - буде сприйматись як якась змінна або просто помилка в синтаксисі мови. Екранування ж, допо-
могає нам уникати таких помилок, і завдяки знаку зворотнього слеша перед апострофом, Javascript вже буде сприймати апостроф
саме як апостроф, тобто як частину строкового значення, а не частину синтаксису мови Javascript.
Також екранування відкриває певні можливості:
- завдяки певним символам розширюється стандартний функціонал мови - наприклад, в подвійних і одинарних лапках
завдяки знаку переноса строки (\n) ми можемо утворити строку, яка розподілятиметься на різних рядках.
- символ екранування також використовується в синтаксисі регулярних виражень.
2. Які засоби оголошення функцій ви знаєте?
- function expression: const func = function() {...};
- function declaration: function someFunc() {...};
- arrow function: const func = () => {...};
- function named expression: const func = danIt() {...};
3. Що таке hoisting, як він працює для змінних та функцій?
hoisting - це особливість інтерпритатора Javascript, яка працює для змінних та функцій, оголошених за допомогою function declaration.
Для функцій оголошених за допомогою function declaration цце означає, що їх можна викликати до того, як вони були оголошені в коді.
"Під капотом" це виглядає так: до початку виконання коду, інтерпритатор пробігається по коду, та шукає функції оголошені за допомогою 
function declaration. Коли він їх знаходить, він вже записує їх в свою пам'ять. І на момент виконання всього коду - вони вже існують і 
виконуються до початку їх оголошення в коді. Що стотується змінних, то hoisting в данному випадку працює тільки з оголошенням змінних
через var. До таких змінних можна звернутися до моменту їх оголошення, але їх значення в такому випадку завжди буде undefined. З let 
та const це не працює - буде помилка.
*/ 
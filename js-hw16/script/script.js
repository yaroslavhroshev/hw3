let validateNumber = number => !(!number || isNaN(number));

function getNumber(message = "Введіть будь яке число") {
    let askNumber;
    do {
        askNumber = prompt(message, askNumber ? askNumber : "");
        if (askNumber !== null) {
            askNumber = askNumber.trim();
        }
    } while (!validateNumber(askNumber))
    return +askNumber
}
let fibonacci = (n, f0 = 0, f1 = 1) => {
    let result = 0;
    if (n === 0 || n === 1) {
        return n;
    }
    if (n > 0) {
        // console.log(f0, f1);
        result = f0 + f1;
        f0 = result - f0; 
        f1 = result;
        // console.log(result);
        if (n <= 2 ) return result
        return fibonacci(n - 1, f0, f1)
    }
    if(n < 0) {
        if (n === -1) return 1;
        if(n % 2 === 0) {
            //    console.log(result)
               result = f0 - f1;
               f0 = result;
               f1 =  f1 - f0;
               if (n >= -2) return result
               return fibonacci(n + 2, f0, f1)
        } else if (n % 2 !== 0) {
                result = f0 - f1;
                f0 = result;
                f1 =  f1 - f0;
                if (n >= -3) return f1
                return fibonacci(n + 2, f0, f1)    
        }
    }
}

const fibo = getNumber();
document.querySelector('p').innerHTML = `Число фібаначі під номером ${fibo} це ${fibonacci(fibo)}`;

